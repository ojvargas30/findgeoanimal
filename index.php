<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>Hola</h2>

    <button type="button" id="btnNatTest">iNaturalist</button>
    <!-- <button type="button" id="btnNatTest2">iNaturalistTokenTwo</button> -->

    <script>
        let btnNatTest = document.getElementById('btnNatTest');
        // btnNatTest2 = document.getElementById('btnNatTest2');

        let clientId = "e4149772c30955721187bed1156d15d29131db3ccc4fe74993d0f76b2f2cae2b",
            secret = "2775bcf92fa057acc2d504c8f156bf1b78bdef8d807715075f218220981d7b49",
            site = "https://www.inaturalist.org",
            redirect_uri = "http://localhost/FindGeoAnimal/index.php",
            url = `${site}/oauth/authorize?client_id=${clientId}&redirect_uri=${redirect_uri}&response_type=code`,
            url3 = `${site}/users/edit.json`,
            urlGetObservations = `${site}/observations.json`,
            headers = {},
            token = '',
            res = '';
        // api_token = "eyJhbGciOiJIUzUxMiJ9.eyJ1c2VyX2lkIjozODY2NTgxLCJleHAiOjE2MDY0OTczMDd9.Q6M6YE6NTZU2taTkTYS4k17UTvpEQe0aqC3dqKQdJEro7hrXbZRXMgEpcnbIJcGTFRuvpk9XmxvuonZJDxxNRQ",


        const handlerRedirect = (e) => { // Que esto se haga solo una vez
            location.href = url // redirección
                // btnNatTest.disabled = true;
                return true
        };

        const init = () => {
            // btnNatTest.addEventListener('click', handlerRedirect)
            if (res == '') {
                if (handlerRedirect()) res = new URLSearchParams(location.search)
            }

            let code = res.get("code"),
                payload = {
                    client_id: clientId,
                    client_secret: secret,
                    code: code,
                    redirect_uri: redirect_uri,
                    grant_type: "authorization_code",
                    "Authorization": "Bearer " + code
                }

            let url2 = `${site}/oauth/token`;

            let opt = {
                method: "POST",
                body: JSON.stringify(payload),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            };

            fetch(url2, opt)
                .then(response => response.json())
                .then(result => {
                    console.log(result)
                    token = result.access_token;
                    console.log(token);

                    opt2 = {
                        method: "GET",
                        headers: {
                            "Authorization": `Bearer ${token}`
                        }
                    }

                    fetch(url3, opt2)
                        .then(response => response.json())
                        .then(result => console.log(result))
                        .catch(e => console.log(e));

                    optObservations = {
                        method: "GET",
                        headers: {
                            "Authorization": `Bearer ${token}`
                        }
                    }

                    fetch(urlGetObservations, optObservations)
                        .then(response => response.json())
                        .then(result => console.log(result))
                        .catch(e => console.log(e));
                })
                .catch(e => console.log(e));
        };

        document.addEventListener('DOMContentLoaded', init);
    </script>
</body>

</html>